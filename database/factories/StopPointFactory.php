<?php

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/**
 * @var Factory $factory
 */
$factory->define(App\Models\StopPoint::class, function (Faker $faker) {
    return [
        'external_id' => $faker->randomNumber(),
        'name' => $faker->name,
    ];
});
