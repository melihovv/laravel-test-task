<?php

declare(strict_types = 1);

namespace App;

use RuntimeException;

class ScheduleParserFromXml
{
    /**
     * @var Schedule
     */
    private $schedule;

    /**
     * @var Graph
     */
    private $currentGraph;

    /**
     * @var Event
     */
    private $currentEvent;

    /**
     * @param string $xmlFilePath
     * @return Schedule
     * @throws RuntimeException
     */
    public function parse(string $xmlFilePath) : Schedule
    {
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);
        xml_set_element_handler($parser, [$this, 'handleStartElement'], [$this, 'handleEndElement']);

        $handle = $this->openFile($xmlFilePath);

        while ($data = fread($handle, 4096)) {
            $parseStatus = xml_parse($parser, $data, feof($handle));

            $this->handleParseStatus($parseStatus, $parser);
        }

        xml_parser_free($parser);

        return $this->schedule ?? new Schedule();
    }

    /**
     * @param resource $parser
     * @param string $name
     * @param array $attrs
     * @throws RuntimeException
     */
    private function handleStartElement($parser, $name, $attrs)
    {
        switch ($name) {
            case 'raspvariant':
                $this->schedule = new Schedule($attrs);
                break;
            case 'graph':
                if (!isset($attrs['num']))  {
                    throw new RuntimeException('<graph> tag without num atrribute');
                }

                $this->currentGraph = new Graph($attrs);
                break;
            case 'event':
                $this->currentEvent = new Event($attrs);
                break;
            case 'stop':
                if ($this->currentEvent->isProduction()) {
                    $this->currentEvent->addStop(new Stop($attrs));
                }
                break;
        }
    }

    /**
     * @param resource $parser
     * @param string $name
     */
    private function handleEndElement($parser, $name)
    {
        switch ($name) {
            case 'graph':
                $this->schedule->addGraph($this->currentGraph->num, $this->currentGraph);
                $this->currentGraph = null;
                break;
            case 'event':
                $this->currentGraph->addEvent($this->currentEvent);
                $this->currentEvent = null;
                break;
        }
    }

    /**
     * @param $parseStatus
     * @param $parser
     * @throws RuntimeException
     */
    private function handleParseStatus($parseStatus, $parser)
    {
        if (!$parseStatus) {
            throw new RuntimeException(
                sprintf(
                    'XML error at line %d column %d',
                    xml_get_current_line_number($parser),
                    xml_get_current_column_number($parser)
                )
            );
        }
    }

    /**
     * @param string $xmlFilePath
     * @return resource
     * @throws RuntimeException
     */
    private function openFile(string $xmlFilePath)
    {
        if (!file_exists($xmlFilePath)) {
            throw new RuntimeException("The specified file [$xmlFilePath] does not exist.");
        }

        $handle = fopen($xmlFilePath, 'rb');
        if (!$handle) {
            throw new RuntimeException("Could not read the specified file [$xmlFilePath].");
        }

        return $handle;
    }
}
