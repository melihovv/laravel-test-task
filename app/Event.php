<?php

declare(strict_types = 1);

namespace App;

class Event
{
    use AttrsContainer;

    /**
     * @var array
     */
    private $stops = [];

    /**
     * @param Stop $stop
     * @return $this
     */
    public function addStop(Stop $stop)
    {
        $this->stops[] = $stop;

        return $this;
    }

    /**
     * @return array
     */
    public function getStops() : array
    {
        return $this->stops;
    }

    /**
     * @return bool
     */
    public function isProduction()
    {
        return $this->ev_id == 4;
    }
}
