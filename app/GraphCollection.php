<?php

declare(strict_types = 1);

namespace App;

use App\Models\StopPoint;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use InvalidArgumentException;

class GraphCollection extends Collection
{
    /**
     * @return int
     */
    public function amountOfEvents()
    {
        return $this->sum(function (Graph $graph) {
            return $graph->amountOfEvents();
        });
    }

    /**
     * @return int in minutes
     * @throws InvalidArgumentException
     */
    public function totalTimeOfEvents()
    {
        return $this->sum(function (Graph $graph) {
            $graphEventsTotalTimeAccumulator = 0;

            foreach ($graph->getEvents() as $event) {
                $end = Carbon::createFromFormat('H:i', $event->end);
                $start = Carbon::createFromFormat('H:i', $event->start);

                $graphEventsTotalTimeAccumulator += $end->diffInMinutes($start);
            }

            return $graphEventsTotalTimeAccumulator;
        });
    }

    /**
     * @param string $from Time from in "hh:mm" format.
     * @param string $to Time to in "hh:mm" format.
     * @return array
     * @throws InvalidArgumentException
     */
    public function allStopsBetween(string $from, string $to)
    {
        $stops = [];
        $extenalIds = [];

        $fromCarbon = Carbon::createFromFormat('H:i', $from);
        $toCarbon = Carbon::createFromFormat('H:i', $to);

        foreach ($this->items as $graph) {
            foreach ($graph->getEvents() as $eventIndex => $event) {
                if (!$event->isProduction()) {
                    continue;
                }

                foreach ($event->getStops() as $stop) {
                    if (Carbon::createFromFormat('H:i', $stop->time)->between($fromCarbon, $toCarbon)) {
                        $stop->graph_number = $graph->num;
                        $stop->event_index = $eventIndex;
                        $stops[$stop->st_id] = $stop;
                        $extenalIds[] = $stop->st_id;
                    }
                }
            }
        }

        foreach (StopPoint::whereIn('external_id', $extenalIds)->get() as $stopPoint) {
            if (!isset($stops[$stopPoint->external_id])) {
                continue;
            }

            $stops[$stopPoint->external_id]->name = $stopPoint->name;
        }

        return array_values($stops);
    }
}
