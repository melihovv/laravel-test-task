<?php

declare(strict_types = 1);

namespace App;

class Schedule
{
    use AttrsContainer;

    /**
     * @var array|GraphCollection[]
     */
    private $graphs = [];

    /**
     * @param $number
     * @param Graph $graph
     * @return $this
     */
    public function addGraph($number, Graph $graph)
    {
        if (isset($this->graphs[$number])) {
            $this->graphs[$number]->push($graph);
        } else {
            $this->graphs[$number] = new GraphCollection([$graph]);
        }

        return $this;
    }

    /**
     * @return array|GraphCollection[]
     */
    public function getGraphs() : array
    {
        ksort($this->graphs);

        return array_values($this->graphs);
    }
}
