<?php

declare(strict_types = 1);

namespace App;

class Graph
{
    use AttrsContainer;

    private $events = [];

    /**
     * @param Event $event
     * @return $this
     */
    public function addEvent(Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * @return array
     */
    public function getEvents() : array
    {
        return $this->events;
    }

    /**
     * @return int
     */
    public function amountOfEvents()
    {
        return count($this->events);
    }
}
