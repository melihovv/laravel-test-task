<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StopPoint extends Model
{
    public $timestamps = false;
}
