<?php

declare(strict_types = 1);

namespace App;

trait AttrsContainer
{
    /**
     * @var array
     */
    private $attrs;

    /**
     * Graph constructor.
     * @param array $attrs
     */
    public function __construct(array $attrs = [])
    {
        $this->attrs = $attrs;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->attrs[$name] ?? null;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        $this->attrs[$name] = $value;

        return $this;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->attrs[$name]);
    }
}
