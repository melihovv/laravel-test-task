<?php

namespace App\Console\Commands;

use App\ScheduleParserFromXml;
use Illuminate\Console\Command;
use Throwable;

class ParseScheduleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:schedule {xml-file-path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse schedule';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $parser = new ScheduleParserFromXml();
            $parser->parse($this->argument('xml-file-path'));
        } catch (Throwable $e) {
            report($e);

            $this->error($e->getMessage());
        }

        return 0;
    }
}
