# Requirements
- php >= 7.0
- composer

# Install
- git clone https://bitbucket.org/melihovv/laravel-test-task
- cd laravel-test-task
- composer install
- cp .env.example .env
- php artisan key:generate

# Run tests
- composer run phpunit

# Run console command
`php artisan parse:schedule /path/to/file.xml`

# Main classes
- App
    - Schedule
    - Graph
    - GraphCollection
    - Event
    - Stop
    - ScheduleParserFromXml
- Tests
    - Unit
        - ScheduleParserFromXmlTest
        - GraphCollectionTest
