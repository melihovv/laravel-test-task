<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\ScheduleParserFromXml;
use App\Schedule;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use RuntimeException;
use Tests\TestCase;

class ScheduleParserFromXmlTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    public function setUp()
    {
        $this->root = vfsStream::setup('root', null, [
            'raspvariant.xml' => file_get_contents(__DIR__ . '/../Fixtures/raspvariant.xml'),
            'two-graphs-with-different-number.xml' => file_get_contents(__DIR__ . '/../Fixtures/two-graphs-with-different-number.xml'),
            'two-graphs-with-the-same-number.xml' => file_get_contents(__DIR__ . '/../Fixtures/two-graphs-with-the-same-number.xml'),
            'two-graphs-ordered-by-number-desc.xml' => file_get_contents(__DIR__ . '/../Fixtures/two-graphs-ordered-by-number-desc.xml'),
            'not-valid.xml' => 'not valid xml',
        ]);
    }

    /** @test */
    function it_returns_instance_of_schedule()
    {
        $graphParser = new ScheduleParserFromXml();
        $schedule = $graphParser->parse($this->root->url() . '/raspvariant.xml');
        $this->assertInstanceOf(Schedule::class, $schedule);
    }

    /**
     * @test
     * @expectedException RuntimeException
     */
    function it_throws_exception_if_it_cannot_read_xml_file()
    {
        $graphParser = new ScheduleParserFromXml();
        $graphParser->parse($this->root->url() . '/not-existent.xml');
    }

    /**
     * @test
     * @expectedException RuntimeException
     */
    function it_throws_exception_if_xml_file_is_not_valid()
    {
        $graphParser = new ScheduleParserFromXml();
        $graphParser->parse($this->root->url() . '/not-valid.xml');
    }

    /** @test */
    function it_successfully_parse_schedule_from_xml()
    {
        $graphParser = new ScheduleParserFromXml();
        $schedule = $graphParser->parse($this->root->url() . '/two-graphs-with-different-number.xml');

        $this->assertCount(2, $schedule->getGraphs());
        $this->assertEquals(17084, $schedule->num);

        $graphs = $schedule->getGraphs();

        $graph = $graphs[0][0];
        $this->assertEquals(1, $graph->num);
        $this->assertEquals(1, $graph->smena);
        $this->assertEquals(3, $graph->getEvents()[0]->ev_id);

        $event = $graph->getEvents()[1];
        $this->assertEquals(4, $event->ev_id);
        $this->assertEquals(50280388, $event->getStops()[0]->st_id);
        $this->assertEquals(50280122, $event->getStops()[1]->st_id);

        $graph = $graphs[1][0];
        $this->assertEquals(2, $graph->num);
        $this->assertEquals(2, $graph->smena);
        $this->assertEquals(14, $graph->getEvents()[0]->ev_id);
        $this->assertEquals(5, $graph->getEvents()[1]->ev_id);
    }

    /** @test */
    function it_successfully_parse_schedule_which_contains_graphs_with_the_same_num()
    {
        $graphParser = new ScheduleParserFromXml();
        $schedule = $graphParser->parse($this->root->url() . '/two-graphs-with-the-same-number.xml');

        $this->assertCount(1, $schedule->getGraphs());
        $this->assertCount(2, $schedule->getGraphs()[0]);
    }

    /** @test */
    function graphs_should_be_sorted_by_number()
    {
        $graphParser = new ScheduleParserFromXml();
        $schedule = $graphParser->parse($this->root->url() . '/two-graphs-ordered-by-number-desc.xml');

        $this->assertCount(2, $schedule->getGraphs());
        $this->assertEquals(1, $schedule->getGraphs()[0][0]->num);
        $this->assertEquals(2, $schedule->getGraphs()[1][0]->num);
    }
}
