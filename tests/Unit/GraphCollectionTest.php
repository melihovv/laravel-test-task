<?php

declare(strict_types = 1);

namespace Tests\Unit;

use App\Event;
use App\Graph;
use App\GraphCollection;
use App\Models\StopPoint;
use App\Stop;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GraphCollectionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_returns_amount_of_events()
    {
        $collection = (new GraphCollection())
            ->push((new Graph)->addEvent(new Event)->addEvent(new Event))
            ->push((new Graph)->addEvent(new Event))
            ->push((new Graph)->addEvent(new Event)->addEvent(new Event))
            ->push(new Graph);

        $this->assertEquals(5, $collection->amountOfEvents());
    }

    /** @test */
    function it_returns_total_time_of_events()
    {
        $collection = (new GraphCollection())
            ->push(
                (new Graph)
                    ->addEvent(new Event(['start' => '04:15', 'end' => '04:45'])) // 30m
                    ->addEvent(new Event(['start' => '04:45', 'end' => '06:07'])) // 82m
            )
            ->push((new Graph)->addEvent(new Event(['start' => '09:25', 'end' => '09:51']))) // 26m
            ->push(new Graph);

        $this->assertEquals(30 + 82 + 26, $collection->totalTimeOfEvents());
    }

    /** @test */
    function it_returns_all_stops_between()
    {
        $collection = (new GraphCollection())
            ->push(
                (new Graph(['num' => 1]))
                    ->addEvent(
                        (new Event(['ev_id' => 4]))
                            ->addStop(new Stop(['time' => '05:15', 'st_id' => '50280387']))
                    )
                    ->addEvent(new Event)
            )
            ->push(
                (new Graph(['num' => 2]))
                    ->addEvent(
                        (new Event(['ev_id' => 4]))
                            ->addStop(new Stop(['time' => '05:23', 'st_id' => '50280388']))
                            ->addStop(new Stop(['time' => '05:36', 'st_id' => '50280389']))
                    )
            )
            ->push(new Graph());

        $sp1 = factory(StopPoint::class)->create(['external_id' => '50280387']);
        $sp2 = factory(StopPoint::class)->create(['external_id' => '50280388']);
        $sp3 = factory(StopPoint::class)->create(['external_id' => '50280389']);

        $stops = $collection->allStopsBetween('05:15', '05:25');
        $this->assertCount(2, $stops);

        $stop = $stops[0];
        $this->assertEquals('50280387', $stop->st_id);
        $this->assertEquals($sp1->name, $stop->name);
        $this->assertEquals(1, $stop->graph_number);
        $this->assertEquals(0, $stop->event_index);

        $stop = $stops[1];
        $this->assertEquals('50280388', $stop->st_id);
        $this->assertEquals($sp2->name, $stop->name);
        $this->assertEquals(2, $stop->graph_number);
        $this->assertEquals(0, $stop->event_index);
    }
}
